import os
import urllib
import base64
import posixpath
import SimpleHTTPServer
import BaseHTTPServer

class Handler(SimpleHTTPServer.SimpleHTTPRequestHandler):
    ''' Main class to present webpages and authentication. '''

    def get_users(self):
        users = {}
        fh = open('./users.txt', 'r')
        fdata = fh.read()
        fh.close()

        lines = fdata.split('\n')
        for line in lines:
            if line:
                username, password = line.split(":")
                users[username] = {
                    "username": username,
                    "password": password,
                    "auth": line,
                    "based": base64.b64encode(line)
                }
                try:
                    os.makedirs("users/%s" % username)
                except:
                    pass
        return users

    def do_HEAD(self):
        print "send HEAD"
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_AUTHHEAD(self):
        print "send AUTHHEAD"
        self.send_response(401)
        self.send_header('WWW-Authenticate', 'Basic realm=\"\"')
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        ''' Present frontpage with user authentication. '''
        auth = self.headers.getheader('Authorization')
        usersdata = self.get_users()
        if auth == None:
            self.do_AUTHHEAD()
            self.wfile.write('no auth header received')
            return

        try:
            #print "auth", auth
            b64 = auth.split(" ")[1]
            #print "b64", b64
            b64decoded = base64.b64decode(b64)
            #print "b64decoded", b64decoded
            username, password = b64decoded.split(':')
        except (TypeError, ValueError):
            self.do_AUTHHEAD()
            self.wfile.write('no auth header received')
            return

        if usersdata.get(username, None) is not None and auth == "Basic %s" % usersdata.get(username, None)['based']:
            #self.do_HEAD()
            self.username = username
            f = self.send_head()
            if f:
                self.copyfile(f, self.wfile)
                f.close()
            pass
        else:
            self.do_AUTHHEAD()
            self.wfile.write(self.headers.getheader('Authorization'))
            self.wfile.write('not authenticated')
            pass

    def translate_path(self, path):
        """Translate a /-separated PATH to the local filename syntax.

        Components that mean special things to the local file system
        (e.g. drive or directory names) are ignored.  (XXX They should
        probably be diagnosed.)

        """
        # abandon query parameters
        path = path.split('?',1)[0]
        path = path.split('#',1)[0]
        # Don't forget explicit trailing slash when normalizing. Issue17324
        trailing_slash = True if path.rstrip().endswith('/') else False
        path = posixpath.normpath(urllib.unquote(path))
        words = path.split('/')
        words = filter(None, words)
        path = os.getcwd() + "/users/%s/" % self.username
        for word in words:
            drive, word = os.path.splitdrive(word)
            head, word = os.path.split(word)
            if word in (os.curdir, os.pardir): continue
            path = os.path.join(path, word)
        if trailing_slash:
            path += '/'
        return path


def main():
    host = ("localhost", 10003)

    server = BaseHTTPServer.HTTPServer(host, Handler)
    #server.handle_request()
    server.serve_forever()

if __name__ == '__main__':
    main()
